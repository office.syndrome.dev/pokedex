import React, { Component } from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

class Header extends Component {
    render() {
        return <Container className="">
            <Row className="justify-content-md-center">
                <Col className="text-center">
                    <h3>My Pokedex</h3>
                </Col>
            </Row>
        </Container>
    }
}

export default Header;