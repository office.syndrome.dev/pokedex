import React, { Component, useState } from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
class ModalAdd extends Component {
    render() {
        return <Modal show={this.props.show}>
            <Modal.Header closeButton>
                <Modal.Title>{this.props.modalTitle}</Modal.Title>
            </Modal.Header>
            <Modal.Body>Woohoo, you're reading this text in a modal!</Modal.Body>
            <Modal.Footer>
                <Button variant="secondary">
                    Close
          </Button>
                <Button variant="primary">
                    Save Changes
          </Button>
            </Modal.Footer>
        </Modal>
    }
}

export default ModalAdd;