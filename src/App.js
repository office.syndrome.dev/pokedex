import React, { Component, useEffect, useState } from 'react'
import './App.css'
import Header from './components/Header';
import ModalAdd from './components/ModalAdd';
import { Container, Row, Col, Card, ProgressBar, Button } from 'react-bootstrap';
import { FaPlus } from 'react-icons/fa';

const COLORS = {
  Psychic: "#ec5656",
  Fighting: "#f0932b",
  Fairy: "#c44569",
  Normal: "#f6e58d",
  Grass: "#badc58",
  Metal: "#95afc0",
  Water: "#3dc1d3",
  Lightning: "#f9ca24",
  Darkness: "#574b90",
  Colorless: "#FFF",
  Fire: "#eb4d4b"
}

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pokemons: [],
      modalTitle: 'Modal Pokemon',
      show: false
    }
  }
  getList() {
    fetch('http://localhost:3030/api/cards')
      .then(res => res.json())
      .then((data) => {
        this.setState({ pokemons: data.cards })
        console.log('Pokemon:--->', this.state.pokemons);
      })
      .catch(console.log)
  }
  showModal = () => {
    this.setState({ show: true });
  }
  componentDidMount() {
    this.getList();
  }
  render() {
    const container = {
      overflowY: 'scroll',
      position: 'absolute',
      top: '50px',
      bottom: '60px',
      card: {
        flexDirection: 'row',
        image: {
          width: 'auto',
          height: '200px'
        }
      }
    }
    const tabbar = {
      container: {
        bottom: 0
      }
    }
    return (
      <div className="App position-relative h-100">
        <Header></Header>
        <Container style={container}>
          <Row className="justify-content-md-center" sm="2" md="2" lg="2">
            {this.state.pokemons.map((pokemon) => (
              <Col className="mb-3">
                <Card bg="Light" style={container.card} className="p-2">
                  <Card.Img src={pokemon.imageUrl} style={container.card.image} />
                  <Card.Body>
                    <Card.Title>{pokemon.name}</Card.Title>
                    <Card.Text><strong>HP</strong>
                      <ProgressBar variant="warning" now={pokemon.hp} />
                    </Card.Text>
                    <Card.Text><strong>STR</strong>
                      <ProgressBar variant="warning" now={pokemon.hp} />
                    </Card.Text>
                    <Card.Text><strong>WEAK</strong>
                      <ProgressBar variant="warning" now={pokemon.hp} />
                    </Card.Text>
                  </Card.Body>
                </Card>
              </Col>
            ))}
          </Row>
        </Container>
        <ModalAdd modalTitle={this.state.modalTitle} show={this.state.show} />
        <Container className="position-absolute bg-danger" style={tabbar.container}>
          <Row className="justify-content-md-center">
            <Col className="text-center">
              <Button variant="danger" className="rounded-circle" onClick={this.showModal}><FaPlus /></Button>
            </Col>
          </Row>
        </Container>
      </div>
    )
  }
}

export default App
